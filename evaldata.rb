require 'ostruct'

def load_data(filename)
  puts "Loading #{filename} ..."
  data = File.read(filename).scan(/[-.eE0-9]+/).map(&:to_f).each_slice(14).to_a
  data.map do |row|
    OpenStruct.new(:alphabet_size => row[0].to_i,
                   :orig_length   => row[1].to_i,
                   :noise_ratio   => row[2],
                   :num_segments  => row[3].to_i,
                   :precision     => row[10],
                   :recall        => row[11],
                   :f1            => row[12],
                   :time          => row[13])
  end
end

def runs_per_case
  10
end

def average_data(filename)
  rows = load_data(filename).each_slice(runs_per_case).map do |rows|
    alphabet_size = rows[0].alphabet_size
    orig_length = rows[0].orig_length
    noise_ratio = rows[0].noise_ratio
    num_segments = rows[0].num_segments
    
    rows.each do |row|
      raise 'alphabet_size error' if row.alphabet_size != alphabet_size
      raise 'orig_length error' if row.orig_length != orig_length
      raise 'noise_ratio error' if row.noise_ratio != noise_ratio
      raise 'num_segments error' if row.num_segments != num_segments
    end
    
    avg_precision = rows.map(&:precision).reduce(:+) / runs_per_case
    avg_recall = rows.map(&:recall).reduce(:+) / runs_per_case
    avg_f1 = rows.map(&:f1).reduce(:+) / runs_per_case
    avg_time = rows.map(&:time).reduce(:+) / runs_per_case

    OpenStruct.new(:alphabet_size => alphabet_size,
                   :orig_length   => orig_length,
                   :noise_ratio   => noise_ratio,
                   :num_segments  => num_segments,
                   :precision     => avg_precision,
                   :recall        => avg_recall,
                   :f1            => avg_f1,
                   :time          => avg_time)
  end
  File.open("#{filename}.avg", "w") do |f|
    rows.each do |row|
      f.write row.alphabet_size
      f.write "\t"
      f.write row.orig_length
      f.write "\t"
      f.write row.noise_ratio
      f.write "\t"
      f.write row.num_segments
      f.write "\t"
      f.write row.precision
      f.write "\t"
      f.write row.recall
      f.write "\t"
      f.write row.f1
      f.write "\t"
      f.write row.time
      f.write "\n"
    end
  end
end

average_data('eval.csv.2.random_shuffled')
average_data('eval.csv.2.not_shuffled')

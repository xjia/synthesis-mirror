local

(* a random number seed used globally *)
val seed = Random.rand (20100521,20130603)

in

fun testOnce (f, alphabetSize, origLength, noiseRatio, numSegments, random) =
let
  (* generate a random number between 0 and n-1 *)
  fun rand n = Random.randNat seed mod n

  (* generate a random list of symbols of length l
     each symbol is in the alphabet 0 to n-1 *)
  fun generate _ 0 = []
    | generate n l = rand n :: generate n (l - 1)

  (* cut s into k pieces without empty ones *)
  fun cutWithoutEmpty 1 s = [s]
    | cutWithoutEmpty k s =
        let val x = 1 + rand (length s - k + 1)
            val piece = List.take (s, x)
            val rest = List.drop (s, x)
        in  piece :: cutWithoutEmpty (k - 1) rest
        end

  (* cut s into k pieces with empty ones *)
  fun cutWithEmpty 1 s = [s]
    | cutWithEmpty k s =
        let val x = rand (length s + 1)
            val piece = List.take (s, x)
            val rest = List.drop (s, x)
        in  piece :: cutWithEmpty (k - 1) rest
        end

  (* randomly shuffle the list xs *)
  fun shuffle xs =
  let
    fun sortBy f xs =
      let
        fun merge ([], ys) = ys : (real * 'a) list
          | merge (xs, []) = xs
          | merge (x::xs, y::ys) =
              if #1(x) <= #1(y) then x::merge(xs, y::ys)
                                else y::merge(x::xs, ys)
        fun sort [] = []
          | sort [x] = [x]
          | sort xs =
              let val k = length xs div 2
              in merge(sort (List.take(xs,k)),
                        sort (List.drop(xs,k)))
              end
      in
        #2(ListPair.unzip (sort (ListPair.zip ((List.map f xs), xs))))
      end
  in
    sortBy (fn _=> Random.randReal seed) xs
  end

  fun interleave [] ys      = ys
    | interleave (x::xs) ys = x :: interleave ys xs

  fun indexes [] i       = [i]
    | indexes ([]::xs) i = indexes xs i
    | indexes (x::xs) i  = i :: indexes xs (i + length x)

  fun member (_,[])    = false
    | member (x,y::ys) = x=y orelse member(x,ys)

  fun remove (_,[])    = []
    | remove (x,y::ys) = if x=y then ys else y::remove(x,ys)

  fun falseNegative [] _       = 0
    | falseNegative (x::xs) ys =
       if member(x,ys) then falseNegative xs ys
                       else falseNegative xs ys + 1

  fun truePositive [] _       = 0
    | truePositive (x::xs) ys =
        if member(x,ys) then truePositive xs ys + 1
                        else truePositive xs ys

  fun falsePositive xs ys = falseNegative ys xs

  fun trueNegative xs ys len =
    let fun merge ([], ys) = ys
          | merge (xs, []) = xs
          | merge(x::xs, y::ys) =
              if x<y then x::merge(xs, y::ys)
              else if x=y then x::merge(xs, ys)
              else y::merge(x::xs, ys)
    in  len - 1 - length (merge(xs,ys))
    end

  fun evaluate' (len,A,B) = { TP = truePositive A B
                            , TN = trueNegative A B len
                            , FP = falsePositive A B
                            , FN = falseNegative A B }

  fun evaluate (len,A,B) =
    evaluate' (len, remove(0,remove(len,A)),
                    remove(0,remove(len,B)))

  val origStr = generate alphabetSize origLength
  val origSegments = cutWithoutEmpty numSegments origStr
  val noiseLength = Real.round (Real.fromInt origLength * Real.abs noiseRatio)
  val noiseStr = generate alphabetSize noiseLength
  val noiseSegments = cutWithEmpty (numSegments + 1) noiseStr
  val origShuffled = if random then shuffle origSegments else origSegments
  val interSegments = interleave noiseSegments origShuffled
  val interStr = List.concat interSegments
in
  (*
  List.map (fn x=>print(Int.toString x ^ " ")) origStr;
  print "\n";
  List.map (fn x=>print(Int.toString x ^ " ")) interStr;
  print "\n";*)
  if noiseRatio >= 0.0
  then evaluate(length origStr, indexes origSegments 0, f(origStr, interStr))
  else evaluate(length interStr, indexes interSegments 0, f(interStr, origStr))
end

end

(*****************************************************************************)

local

fun disjointSegmentsCombinations k s =
  let
    (* separate a string into two parts *)
    fun pairs [] = []
      | pairs (x::xs) = ([x],xs)::(List.map (fn(l,r)=>(x::l,r)) (pairs xs))

    (* f(k,s,p) contains all cases to draw k segments from s *)
    fun f(0,_,_) = [[]]
      | f(_,[],_) = []
      | f(k,s,p) = g(k,s,p) @ f(k,tl s,p+1)
    (* g(k,s,p) contains all cases to draw k segments from s,
       and the 1st segment starts exactly at the begining of
       s, i.e. at position p *)
    and g(0,_,_) = [[]]
      | g(_,[],_) = []
      | g(k,s,p) =
          List.concat (List.map (fn(l,r)=> List.map (fn x => (l,p)::x)
                                                    (f(k-1,r,p + length l)))
                                (pairs s))
in f(k,s,0)
end

fun correspondences k F input output =
  let
    val combs = disjointSegmentsCombinations k input

    (* ('a list * int) list * (''b list * int) *)
    fun correspondence matches output = (matches, output)

    fun matches output pos = List.map (fn x => correspondence x (output,pos))
                                        (List.filter (matches' F output) combs)
    and matches' [] _ _ = false
      | matches' (f::fs) x y = f(List.map #1 y)=x orelse matches' fs x y

    (* remove last element of s *)
    fun shorter s = (rev o tl o rev) s

    (* `try` returns a pair of matches and a shift length *)
    fun try [] _ = ([],1)
      | try output pos = case matches output pos of
                              [] => try (shorter output) pos
                            | ms => (ms,length output)

    fun overlap x = overlap' x 0
    and overlap' [] _ = []
      | overlap' output pos = #1(try output pos) :: overlap' (tl output) (pos+1)

    fun disjoint x = disjoint' x 0
    and disjoint' [] _ = []
      | disjoint' output pos = let val (ms,len) = try output pos
                               in ms :: disjoint' (List.drop (output,len)) pos
                               end
  in
    (List.concat o disjoint) output
  end

fun mergesort [] = []
  | mergesort [x] = [x]
  | mergesort xs =
      let val k = length xs div 2
          fun merge([],ys) = ys
            | merge(xs,[]) = xs
            | merge(x::xs, y::ys) =
                if x<=y then x::merge(xs, y::ys)
                        else y::merge(x::xs, ys)
      in merge(mergesort (List.take(xs,k)),
               mergesort (List.drop(xs,k)))
      end

(* input list should be in order *)
fun unique [] = []
  | unique [x] = [x]
  | unique (x::y::xs) = if x=y then unique(x::xs)
                               else x::unique(y::xs)

type correspondence = (int list * int) list * (int list * int)

(* return a list of distinctive cutting indexes including zero *)
fun positionIndexesOf' (correspondences : correspondence list) =
  let fun indexes (s,p) = [p, p + length s]
  in (unique o mergesort
              o List.concat
              o (List.map indexes)
              o List.concat
              o (List.map #1)) correspondences
  end

(* remove zero if it appears *)
fun positionIndexesOf (correspondences : correspondence list) =
  case positionIndexesOf' correspondences of
       0::rest => rest
     | indexes => indexes

in

fun ftest (input, output) =
  positionIndexesOf (correspondences 1 [fn[x]=>x] input output)

end

fun top () =
let
  val f = TextIO.openOut "eval.csv"
in
  List.map (fn alphabetSize =>
  List.map (fn origLength =>
  List.map (fn noiseRatio =>
  List.map (fn k =>
  List.map (fn random =>
  List.map (fn t =>
    let val _ = print ( "(" ^ Int.toString alphabetSize
                      ^ "," ^ Int.toString origLength
                      ^ "," ^ Real.toString noiseRatio
                      ^ "," ^ Int.toString k
                      ^ "," ^ Bool.toString random
                      ^ ")"
                      ^ " #" ^ Int.toString t
                      ^ "\n" )
        val timer = Timer.startRealTimer ()
        val stats = testOnce(ftest,alphabetSize,origLength,noiseRatio,k,random)
        val secs = Time.toReal (Timer.checkRealTimer timer)
        val TP = Real.fromInt (#TP(stats))
        val FP = Real.fromInt (#FP(stats))
        val TN = Real.fromInt (#TN(stats))
        val FN = Real.fromInt (#FN(stats))
        val p = if TP+FP<=0.0 then 1.0 else TP/(TP+FP)
        val r = if TP+FN<=0.0 then 1.0 else TP/(TP+FN)
        val f1 = if r+p<=0.0 then 0.0 else 2.0 * r * p / (r + p)
    in  TextIO.output (f, Int.toString alphabetSize
                 ^ "\t" ^ Int.toString origLength
                 ^ "\t" ^ Real.toString noiseRatio
                 ^ "\t" ^ Int.toString k
                 ^ "\t" ^ Bool.toString random
                 ^ "\t" ^ Int.toString t
                 ^ "\t" ^ Real.toString TP
                 ^ "\t" ^ Real.toString FP
                 ^ "\t" ^ Real.toString TN
                 ^ "\t" ^ Real.toString FN
                 ^ "\t" ^ Real.toString p
                 ^ "\t" ^ Real.toString r
                 ^ "\t" ^ Real.toString f1
                 ^ "\t" ^ Real.toString secs
                 ^ "\n");
        TextIO.flushOut f
    end
  ) [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] (* run each case for 10 times *)
  ) [true, false] (* randomly shuffle or not *)
  ) [5, 10, 15, 20] (* number of segments *)
  ) [~0.5, ~0.4, ~0.3, ~0.2, ~0.1, 0.1, 0.2, 0.3, 0.4, 0.5] (* noise ratio *)
  ) [20, 40, 60, 80, 100] (* original length *)
  ) [100, 200, 300, 400, 500, 600, 700, 800, 900,
     1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000] (* alphabet size *)
  ;
  TextIO.closeOut f
end


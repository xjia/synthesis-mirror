signature MYLIST =
sig
  type 'a T
  val new : 'a -> 'a T
  val add : 'a T -> 'a -> 'a T
  val len : 'a T -> int
end

functor Test (L : MYLIST) =
struct
  fun test l =
  let
    val l = L.add l 1
    val _ = print (Int.toString (L.len l) ^ "\n")
    val l = L.add l 2
    val _ = print (Int.toString (L.len l) ^ "\n")
  in
    l
  end
end

structure MyList =
struct
  type 'a T = 'a list
  fun new x = [x]
  fun add xs x = x::xs
  fun len xs = length xs
end


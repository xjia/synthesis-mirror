fun matchMatrix (input, output) =
  let fun matchRow []      _ = []
        | matchRow (x::xs) y = if x=y then 1::(matchRow xs y)
                                      else 0::(matchRow xs y)
  in List.map (matchRow input) output
  end

fun highlightMatrix rows =
  let
    fun find ([], _, _, _, minidx)          = minidx
      | find (x::xs, i, k, mindist, minidx) =
          if x>0 andalso abs(i-k)<=mindist
            then find(xs, i+1, k, abs(i-k), i)
            else find(xs, i+1, k, mindist, minidx)

    fun replace ([], _, _)    = []
      | replace (x::xs, i, k) = if i=k then 2::xs
                                       else x::replace(xs, i+1, k)

    fun highlight ([], _)              = []
      | highlight (row::rows, lastidx) =
          let val curidx = find(row, 1, lastidx, length row, 0)
              val row' = replace(row, 1, curidx)
          in
            if curidx=0 then row'::highlight(rows, lastidx)
                        else row'::highlight(rows, curidx)
          end
  in
    highlight(rows, 0)
  end

(* separate a string into two parts *)
fun pairs [] = []
  | pairs (x::xs) = ([x],xs)::(List.map (fn(l,r)=>(x::l,r)) (pairs xs))

(* f(k,s,p) contains all cases to draw k segments from s *)
fun f(0,_,_) = [[]]
  | f(_,[],_) = []
  | f(k,s,p) = g(k,s,p) @ f(k,tl s,p+1)
(* g(k,s,p) contains all cases to draw k segments from s, and the 1st segment
 * starts exactly at the begining of s, i.e. at position p *)
and g(0,_,_) = [[]]
  | g(_,[],_) = []
  | g(k,s,p) = List.concat (List.map (fn(l,r)=> List.map (fn x => (l,p)::x)
                                                         (f(k-1,r,p + length l)))
                                     (pairs s))

fun disjointSegmentsCombinations k s = f(k,s,0)


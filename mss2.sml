fun mss S X Y =
let
  datatype direction = left | diag

  fun row (_, _, _, []) = []
    | row (_, [], _, _) = raise Fail "last row is empty"
    | row (leftc, diagc::lastrow, x, y::ys) =
        if leftc >= diagc + S(x,y)
        then (left, leftc) :: row (leftc, lastrow, x, ys)
        else (diag, diagc + S(x,y)) :: row (diagc + S(x,y), lastrow, x, ys)

  fun rows ([], _, _) = []
    | rows (_, [], _) = raise Fail "parameter mismatch"
    | rows (x::xs, ys, lastrow) =
        let val r = row (Real.negInf, lastrow, x, ys)
        in  r :: rows (xs, tl ys, List.map #2 r)
        end

  fun removeFirstCell []  = []
    | removeFirstCell row = tl row

  val removeFirstColumn = List.map removeFirstCell

  fun print (_, [], _)             = []
    | print ([], xs, ys)           = raise Match
    | print (_, xs, [])            = raise Match
    | print ([]::restrows, xs, ys) = raise Match
    | print (((diag,_)::restcells)::restrows, x::xs, y::ys) =
        y :: print (removeFirstColumn restrows, xs, ys)
    | print (((left,_)::restcells)::restrows, x::xs, y::ys) =
        print (restcells :: removeFirstColumn restrows, x::xs, ys)

  val table = rows (X, Y, List.tabulate(1 + length Y, fn _=>0.0))
  val table = List.map rev (rev table)
in
  rev (print (table, rev X, rev Y))
end

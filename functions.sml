fun assertEqual x y = if x=y then () else raise Match

exception NotApplicable

local
  fun monthify' 1 = "January"
    | monthify' 2 = "February"
    | monthify' 3 = "March"
    | monthify' 4 = "April"
    | monthify' 5 = "May"
    | monthify' 6 = "June"
    | monthify' 7 = "July"
    | monthify' 8 = "August"
    | monthify' 9 = "September"
    | monthify' 10 = "October"
    | monthify' 11 = "November"
    | monthify' 12 = "December"
    | monthify' _ = raise NotApplicable
in
  fun monthify s = let val SOME n = Int.fromString s
                   in  monthify' n
                   end
                   handle Bind => raise NotApplicable
end

val _ = assertEqual (monthify "6") "June"
val _ = print "[OK] monthify\n"

local
  fun demonthify' "January" = 1
    | demonthify' "February" = 2
    | demonthify' "March" = 3
    | demonthify' "April" = 4
    | demonthify' "May" = 5
    | demonthify' "June" = 6
    | demonthify' "July" = 7
    | demonthify' "August" = 8
    | demonthify' "September" = 9
    | demonthify' "October" = 10
    | demonthify' "November" = 11
    | demonthify' "December" = 12
    | demonthify' _ = raise NotApplicable
in
  fun demonthify s = Int.toString (demonthify' s)
end

val _ = assertEqual (demonthify "June") "6"
val _ = print "[OK] demonthify\n"

fun ordinalize s = let val SOME n = Int.fromString s
                   in  case n mod 10 of
                            1 => (Int.toString n) ^ "st"
                          | 2 => (Int.toString n) ^ "nd"
                          | 3 => (Int.toString n) ^ "rd"
                          | _ => (Int.toString n) ^ "th"
                   end
                   handle Bind => raise NotApplicable

val _ = assertEqual (ordinalize "1") "1st"
val _ = assertEqual (ordinalize "2") "2nd"
val _ = assertEqual (ordinalize "3") "3rd"
val _ = assertEqual (ordinalize "4") "4th"
val _ = print "[OK] ordinalize\n"

fun deordinalize s = case Int.fromString s of
                          SOME n => Int.toString n
                        | NONE => raise NotApplicable

val _ = assertEqual (deordinalize "1st") "1"
val _ = assertEqual (deordinalize "2nd") "2"
val _ = assertEqual (deordinalize "3rd") "3"
val _ = assertEqual (deordinalize "4th") "4"
val _ = print "[OK] deordinalize\n"

fun uppercase s = String.map Char.toUpper s

val _ = assertEqual (uppercase "aditya") "ADITYA"
val _ = print "[OK] uppercase\n"

fun lowercase s = String.map Char.toLower s

val _ = assertEqual (lowercase "Aditya") "aditya"
val _ = assertEqual (lowercase "ADITYA") "aditya"
val _ = print "[OK] lowercase\n"

fun capitalize s = let val c::cs = explode s
                   in  implode ((Char.toUpper c)::cs)
                   end

val _ = assertEqual (capitalize "aditya") "Aditya"
val _ = print "[OK] capitalize\n"

fun weekday s =
let
  val [d,m,y] = String.tokens Char.isSpace s
  val SOME d = Int.fromString d
  val SOME m = Int.fromString (demonthify m)
  val SOME y = Int.fromString y
  val t = [0,3,2,5,0,3,5,1,4,6,2,4]
  val y = if m<3 then y-1 else y
  val w = y + y div 4 - y div 100 + y div 400 + List.nth(t,m-1) + d
in
  case w mod 7 of
       0 => "Sunday"
     | 1 => "Monday"
     | 2 => "Tuesday"
     | 3 => "Wednesday"
     | 4 => "Thursday"
     | 5 => "Friday"
     | 6 => "Saturday"
     | _ => raise Match
end
handle Bind => raise NotApplicable

val _ = assertEqual (weekday "24 June 2010") "Thursday"
val _ = assertEqual (weekday "23 June 2010") "Wednesday"
val _ = print "[OK] weekday\n"


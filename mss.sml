(* fn : ('a * 'b -> real) -> 'a list -> 'b list -> 'b list *)
fun mss S X Y =
  let
    datatype direction = topleft | up
  
    fun col [] _ _ _                  = []
      | col (y::ys) x lastcol lastrow =
          let val v = hd lastcol + S(x,y)
          in  if v>lastrow then (topleft,v)::col ys x (tl lastcol) v
                           else (up,lastrow)::col ys x (tl lastcol) lastrow
          end
  
    fun cols [] _ _             = []
      | cols (x::xs) ys lastcol =
          let val r = col ys x lastcol Real.negInf
          in  r :: cols xs ys (Real.negInf :: List.map #2 r)
          end
  
    (* reconstruct a column after dropping first $n$ elements,
       which ensures it tracks the trace diagonally *)
    fun recol ((topleft,_)::_, y::_, n) = (y,n+1)
      | recol ((up,_)::rest, _::ys, n) = recol (rest,ys,n+1)
      | recol ([], _, _) = raise Match
      | recol (_, [], _) = raise Match
  
    fun recols [] _ = []
      | recols (col::cols) dropCount =
          let val (y,dropCount') = recol (List.drop(col,dropCount),
                                          List.drop(rev Y,dropCount),
                                          dropCount)
          in  y :: recols cols dropCount'
          end

    (* all the borders are filled with -inf while (0,0) is filled with 0 *)
    val r = cols X Y (0.0 :: List.map (fn _=>Real.negInf) Y)
    val r = rev (List.map rev r)
  in
    rev (recols r 0)
  end


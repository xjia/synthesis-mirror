fun correspondences k F input output =
  let
    val kSegments = disjointSegmentsCombinations k input

    (* ('a list * int) list * (''b list * int) *)
    fun correspondence matches output = (matches, output)

    fun matches  output pos     = List.map (fn x => correspondence x (output,pos))
                                           (List.filter (matches' F output) kSegments)
    and matches' [] _ _         = false
      | matches' (f::fs) x comb = f(List.map #1 comb)=x orelse matches' fs x comb

    (* remove last element of s *)
    fun shorter s = (rev o tl o rev) s

    (* `try` returns a pair of matches and a shift length *)
    fun try [] _       = ([],1)
      | try output pos = case matches output pos of
                              [] => try (shorter output) pos
                            | ms => (ms,length output)

    fun overlap  x          = overlap' x 0
    and overlap' [] _       = []
      | overlap' output pos = #1(try output pos) :: overlap' (tl output) (pos+1)

    fun disjoint  x          = disjoint' x 0
    and disjoint' [] _       = []
      | disjoint' output pos = let val (ms,len) = try output pos
                               in  ms :: disjoint' (List.drop (output,len)) pos
                               end
  in
    (List.concat o disjoint) output
  end

val _ = correspondences 2 [fn[x,y]=>y@[#" "]@x]
                        (explode "hello, world")
                        (explode "world hello")
(*
val it =
  [[([#"w",#"o",#"r",#"l",#"d",#" ",#"h",#"e",#"l",#"l",#"o"],0),
    ([#"h",#"e",#"l",#"l",#"o"],0),([#"w",#"o",#"r",#"l",#"d"],7)]]
  : (char list * int) list list

Data structure format:
  [ [(output sublist,offset), (input sublist,offset), ...] (* crsp#1 *)
  , [(output sublist,offset), (input sublist,offset), ...] (* crsp#2 *)
  , ...
  ]

It's a list of correspondences.  A correspondence is a list, 
whose head is the output substring, and the tail is a list of
length $k$, each of the elements is a substring of the input.
A substring is represented as a pair of the string content
and the starting offset of this content in the whole string.

NOTE.  The argument list of length $k$ is in the same order
they appearing in the input.  More complicated considerations
include trying permutations of the arguments (as opposed to
combinations employed in the current implementation).
*)

fun mergesort []  = []
  | mergesort [x] = [x]
  | mergesort xs  =
      let
        val k = length xs div 2
        fun merge([],ys)        = ys
          | merge(xs,[])        = xs
          | merge(x::xs, y::ys) =
              if x<=y then x::merge(xs, y::ys)
                      else y::merge(x::xs, ys)
      in
        merge(mergesort (List.take(xs,k)),
              mergesort (List.drop(xs,k)))
      end

(* input list should be in order *)
fun unique []         = []
  | unique [x]        = [x]
  | unique (x::y::xs) = if x=y then unique(x::xs)
                               else x::unique(y::xs)

(* Input:  a structure (list) of correspondences
   Output: a list of distinctive cutting positions *)
fun correspondencesToPositions xs =
  let
    fun f []          = []
      | f (x::xs)     = g (tl x) :: f xs
    and g []          = []
      | g ((s,p)::xs) = p::(p + length s)::(g xs)
  in
    (unique o mergesort o List.concat o f) xs
  end

val _ = correspondencesToPositions [[([#"E",#"y"],0), ([#"E",#"y"],0)],
                                    [([#"E",#"y"],0), ([#"E",#"y"],6)],
                                    [([#"+"],     0), ([#"+"],     5)],
                                    [([#"="],     0), ([#"="],     2)],
                                    [([#"S",#"y"],0), ([#"S",#"y"],3)],
                                    [([#";"],     0), ([#";"],     8)]]


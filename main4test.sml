signature SYNTHESIS_PROBLEM = sig
  (* input alphabet *)
  type X

  (* output alphabet *)
  eqtype Y

  (* substrings *)
  type input_substr = X list
  type output_substr = Y list
  val toString : input_substr -> string

  (* useful functions *)
  type function = input_substr list -> output_substr
  val functionArities : int list
  val functionsOfArity : int -> function list

  (* character generalization hierarchy *)
  type G
  val generalize : X -> G
  val similarity : G * G -> real
  val combine    : G * G -> G
  val matchChar  : G * X -> bool
end

functor SynthesizerFn (P : SYNTHESIS_PROBLEM) = struct

fun disjointSegmentsCombinations k s =
  let
    (* separate a string into two parts *)
    fun pairs [] = []
      | pairs (x::xs) = ([x],xs)::(List.map (fn(l,r)=>(x::l,r)) (pairs xs))

    (* f(k,s,p) contains all cases to draw k segments from s *)
    fun f(0,_,_) = [[]]
      | f(_,[],_) = []
      | f(k,s,p) = g(k,s,p) @ f(k,tl s,p+1)
    (* g(k,s,p) contains all cases to draw k segments from s,
       and the 1st segment starts exactly at the begining of
       s, i.e. at position p *)
    and g(0,_,_) = [[]]
      | g(_,[],_) = []
      | g(k,s,p) =
          List.concat (List.map (fn(l,r)=> List.map (fn x => (l,p)::x)
                                                    (f(k-1,r,p + length l)))
                                (pairs s))
in  f(k,s,0)
end

fun correspondences k F input output =
  let
    val combs = disjointSegmentsCombinations k input

    (* ('a list * int) list * (''b list * int) *)
    fun correspondence matches output = (matches, output)

    fun matches  output pos  = List.map (fn x => correspondence x (output,pos))
                                        (List.filter (matches' F output) combs)
    and matches' [] _ _      = false
      | matches' (f::fs) x y = f(List.map #1 y)=x orelse matches' fs x y

    (* remove last element of s *)
    fun shorter s = (rev o tl o rev) s

    (* `try` returns a pair of matches and a shift length *)
    fun try [] _       = ([],1)
      | try output pos = case matches output pos of
                              [] => try (shorter output) pos
                            | ms => (ms,length output)

    fun overlap  x          = overlap' x 0
    and overlap' [] _       = []
      | overlap' output pos = #1(try output pos) :: overlap' (tl output) (pos+1)

    fun disjoint  x          = disjoint' x 0
    and disjoint' [] _       = []
      | disjoint' output pos = let val (ms,len) = try output pos
                               in  ms :: disjoint' (List.drop (output,len)) pos
                               end
  in
    (List.concat o disjoint) output
  end

fun mergesort []  = []
  | mergesort [x] = [x]
  | mergesort xs  =
      let val k = length xs div 2
          fun merge([],ys)        = ys
            | merge(xs,[])        = xs
            | merge(x::xs, y::ys) =
                if x<=y then x::merge(xs, y::ys)
                        else y::merge(x::xs, ys)
      in  merge(mergesort (List.take(xs,k)),
                mergesort (List.drop(xs,k)))
      end

(* input list should be in order *)
fun unique []         = []
  | unique [x]        = [x]
  | unique (x::y::xs) = if x=y then    unique(x::xs)
                               else x::unique(y::xs)

type correspondence = (P.input_substr * int) list * (P.output_substr * int)

(* return a list of distinctive cutting indexes including zero *)
fun positionIndexesOf' (correspondences : correspondence list) =
  let fun indexes (s,p) = [p, p + length s]
  in  (unique o mergesort
              o List.concat
              o (List.map indexes)
              o List.concat
              o (List.map #1)) correspondences
  end

(* remove zero if it appears *)
fun positionIndexesOf (correspondences : correspondence list) =
  case positionIndexesOf' correspondences of
       0::rest => rest
     | indexes => indexes

fun positionConjectures (input : P.input_substr, output : P.output_substr) =
  let fun kcrsp k = correspondences k (P.functionsOfArity k) input output
      val correspondences = List.concat (List.map kcrsp P.functionArities)
      val positionIndexes = positionIndexesOf correspondences
      fun positionPair i = (List.take(input,i), List.drop(input,i))
      val positionPairs = List.map positionPair positionIndexes
      fun generalize s = List.map P.generalize s
      fun generalizePair (l,r) = (generalize l, generalize r)
  in  (List.map generalizePair positionPairs, positionIndexes)
  end

(* fn : ('a * 'b -> real) -> 'a list -> 'b list -> 'b list *)
fun mostSimilarSubsequence S X Y =
let
  datatype direction = left | diag

  fun row (_, _, _, []) = []
    | row (_, [], _, _) = raise Fail "last row is empty"
    | row (leftc, diagc::lastrow, x, y::ys) =
        if leftc >= diagc + S(x,y)
        then (left, leftc) :: row (leftc, lastrow, x, ys)
        else (diag, diagc + S(x,y)) :: row (diagc + S(x,y), lastrow, x, ys)

  fun rows ([], _, _) = []
    | rows (_, [], _) = raise Fail "parameter mismatch"
    | rows (x::xs, ys, lastrow) =
        let val r = row (Real.negInf, lastrow, x, ys)
        in  r :: rows (xs, tl ys, List.map #2 r)
        end

  fun removeFirstCell []  = []
    | removeFirstCell row = tl row

  val removeFirstColumn = List.map removeFirstCell

  fun print (_, [], _)             = []
    | print ([], xs, ys)           = raise Match
    | print (_, xs, [])            = raise Match
    | print ([]::restrows, xs, ys) = raise Match
    | print (((diag,_)::restcells)::restrows, x::xs, y::ys) =
        y :: print (removeFirstColumn restrows, xs, ys)
    | print (((left,_)::restcells)::restrows, x::xs, y::ys) =
        print (restcells :: removeFirstColumn restrows, x::xs, ys)

  val table = rows (X, Y, List.tabulate(1 + length Y, fn _=>0.0))
  val table = List.map rev (rev table)
in
  rev (print (table, rev X, rev Y))
end

(* Merge sort *)
fun sortBy f xs =
  let
    fun merge ([], ys)       = ys : (int * 'a) list
      | merge (xs, [])       = xs
      | merge (x::xs, y::ys) =
          if #1(x) <= #1(y) then x::merge(xs, y::ys)
                            else y::merge(x::xs, ys)
    fun sort []  = []
      | sort [x] = [x]
      | sort xs  =
          let val k = length xs div 2
          in  merge(sort (List.take(xs,k)),
                    sort (List.drop(xs,k)))
          end
  in
    #2(ListPair.unzip (sort (ListPair.zip ((List.map f xs), xs))))
  end

type poconj = P.G list * P.G list

(* position conjecture similarity *)
fun poconjSimilarity ((l1,r1) : poconj, (l2,r2) : poconj) =
  let val l = List.foldl op+ 0.0 (ListPair.map P.similarity (rev l1, rev l2))
      val r = List.foldl op+ 0.0 (ListPair.map P.similarity (r1, r2))
  in  (print (Real.toString l ^ " " ^ Real.toString r ^ "\n");
       l + r)
  end

fun poconjCombinator ((l1,r1) : poconj, (l2,r2) : poconj) =
  let val l = ListPair.map P.combine (rev l1, rev l2)
      val r = ListPair.map P.combine (r1, r2)
  in  (rev l, r)
  end

fun poconjIntersection (c1 : poconj list, c2 : poconj list) =
  if length c1 > length c2
  then poconjIntersection (c2, c1)
  else let val c2' = mostSimilarSubsequence poconjSimilarity c1 c2
       in  ListPair.map poconjCombinator (c1, c2')
       end

fun positionConjecture examples =
  let val conjs = List.map positionConjectures examples
      val allPairs = List.map #1 conjs
      val allIndexes = List.map #2 conjs
  in  case sortBy length allPairs of
           c::cs => (List.map (fn x=>print (Int.toString (length x) ^ "\n")) (c::cs);
                     (List.foldl poconjIntersection c cs, allIndexes))
         | []    => raise Match
  end

fun matchPattern (l,r) (x::xs) =
  let fun matches [] _           = true
        | matches _ []           = false
        | matches pattern target = ListPair.all P.matchChar (pattern,target)
      exception NotMatch
      fun mp (l,r) revback forward p =
        if matches l revback andalso matches r forward
          then (print ("p=" ^ Int.toString p ^ "\n"); p)
          else case forward of
                    x::xs => mp (l,r) (x::revback) xs (p+1)
                  | []    => raise NotMatch
  in  mp (l,r) [x] xs 1
  end

fun disambiguatePositionConjecture trainInputs indexes conj =
  let val _ = (
        (* BEGIN DEBUG *)
        print "trainInputs:\n";
        List.map (fn s=> print (P.toString s ^ "\n")) trainInputs;
        print "indexes:";
        List.map (fn i=> print ("  " ^ Int.toString i)) indexes;
        print "\n"
        (* END DEBUG *)
      )

      fun checkGuessForInput g (input, x) =
        let val matchResult = matchPattern g input
            val _ = print ("matchResult=" ^ Int.toString matchResult ^ "\n")
        in  matchResult = x
        end
        handle NotMatch => false

      fun checkGuess g = ListPair.all (checkGuessForInput g)
                                      (trainInputs, indexes)

      fun allGuesses (l,r) =
        let val (l,r) = (rev l,r)
            fun f (x,y,m,n) =
              if  x+y>m+n then []
              else if y>n then        f(x+1,y-1,m,n)
              else if x=m then (x,y)::f(0,x+y+1,m,n)
              else if y=0 then (x,y)::f(0,x+y+1,m,n)
                          else (x,y)::f(x+1,y-1,m,n)
            val pairs = f (0,1,length l,length r)
        in  List.map (fn(x,y)=>(List.take(l,x),List.take(r,y))) pairs
        end
  in  case List.filter checkGuess (allGuesses conj) of
           g::_ => g
         | []   => raise Fail "no suitable guess"
  end

fun pd [] _ _ _ = []
  | pd (conj::conjs) inputs indexesList prevSums =
      let val _ = (
            let val (l,r) = conj
            in  print ("pd conj: ("
                    ^ (Int.toString (length l))
                    ^ ","
                    ^ (Int.toString (length r))
                    ^ ")\n")
            end
          )
          val indexes = List.map hd indexesList
          val relativeIndexes = ListPair.map op- (indexes, prevSums)
          val g = disambiguatePositionConjecture inputs relativeIndexes conj
          val remainInputs = ListPair.map List.drop (inputs, relativeIndexes)
          val remainIndexesList = List.map tl indexesList
          val nextSums = ListPair.map op+ (prevSums, relativeIndexes)
      in  g :: pd conjs remainInputs remainIndexesList nextSums
      end

fun parserDisambiguation conjs indexes inputs testInputs =
  let val conditions = pd conjs inputs indexes (List.map (fn _=>0) inputs)
      fun parser input =
        let fun parse [] _ = []
              | parse _ [] = []
              | parse (c::cs) input =
                  let val pos = matchPattern c input
                      val tok = List.take(input,pos)
                      val rest = List.drop(input,pos)
                  in  tok::parse cs rest
                  end
        in  parse conditions input
        end
  in  parser
  end

fun parser examples testInputs =
  let val (conjecture, indexes) = positionConjecture examples
      val trainInputs = List.map #1 examples
  in  parserDisambiguation conjecture indexes trainInputs testInputs
  end

(* TODO
fun outputTransducer examples parser =
*)

fun synthesizedProgram examples testInputs =
  let val _ = print "\nExamples:\n"
      val _ = List.map (fn(i,_)=> print (P.toString i ^ "\n")) examples
      val parser = parser examples testInputs
      (*val outputTransducer = outputTransducer examples parser
  in  outputTransducer o parser*)
  in  parser
  end

end

structure TrivialSynthesisProblem : SYNTHESIS_PROBLEM =
struct
  (* input alphabet *)
  type X = char

  (* output alphabet *)
  type Y = char

  (* substrings *)
  type input_substr = char list
  type output_substr = char list
  fun toString x = implode x

  (* useful functions *)
  type function = input_substr list -> output_substr
  val functionArities = [1]
  fun functionsOfArity k =
    case k of
         1 => [fn [x]=>x | _=>raise Match]
       | _ => raise Match

  (* character generalization hierarchy *)
  datatype G = Char of X | Any
  fun generalize x = Char x
  fun similarity (Char x, Char y) = if x=y then 1.0 else 0.0
    | similarity (Char _, Any)    = 0.5
    | similarity (Any, Char _)    = 0.5
    | similarity (Any, Any)       = 1.0
  fun combine (Char x, Char y) = if x=y then Char x else Any
    | combine (Char _, Any)    = Any
    | combine (Any, Char _)    = Any
    | combine (Any, Any)       = Any
  fun matchChar (Char x, y) = x=y
    | matchChar (Any, _)    = true
end

structure TrivialSynthesizer = SynthesizerFn (TrivialSynthesisProblem)

val trivialTest0 = TrivialSynthesizer.synthesizedProgram
                     [(explode "$Ey = Sy + Ey;", explode "Ey[i] += Sy;")]
                     []
val trivialTestP0 = trivialTest0 (explode "$Ey = Sy + Ey;")

val trivialTest1 = TrivialSynthesizer.synthesizedProgram
                     [(explode "Ey = Sy + Ey;", explode "Ey[i] += Sy;")]
                     []
val trivialTestP1 = trivialTest1 (explode "Ey = Sy + Ey;")

val trivialTest2 = TrivialSynthesizer.synthesizedProgram
                     [(explode "Ey = Sy + Ey;", explode "Ey[i] += Sy;"),
                      (explode "Ez = Sz + Ez;", explode "Ez[i] += Sz;")]
                     []
val trivialTestP2 = [trivialTest2 (explode "Ey = Sy + Ey;"),
                     trivialTest2 (explode "Ez = Sz + Ez;")]

val trivialTest3 = TrivialSynthesizer.synthesizedProgram
                     [(explode "Ey = Sy + Ey;", explode "Ey[i] += Sy;"),
                      (explode "Efw = Sfw * Efw;", explode "Efw[i] *= Sfw;")]
                     []
val trivialTestP3 = [trivialTest3 (explode "Ey = Sy + Ey;"),
                     trivialTest3 (explode "Efw = Sfw * Efw;")]

val ebeParser =
  TrivialSynthesizer.synthesizedProgram
    [(explode "Yankees 3, Orioles 1.",
      explode "Game[winner 'Yankees', loser 'Orioles', scores [3, 1]];"),
     (explode "Brewers 12, Cardinals 5.",
      explode "Game[winner 'Brewers', loser 'Cardinals', scores [12, 5]];"),
     (explode "Dodgers 5, Braves 4.",
      explode "Game[winner 'Dodgers', loser 'Braves', scores [5, 4]];")]
    []
